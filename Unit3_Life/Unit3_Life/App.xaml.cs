﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Unit3_Life
{
    public class Page1 : ContentPage
    {
        public Page1()
        {
            
            Button btn = new Button() { Text = "Go To Page2" };
            btn.Clicked += async (sender, e) =>
             {
                 await this.Navigation.PushAsync(new Page2());
             };
            this.Content = btn;


        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

    }

    public class Page2 : ContentPage
    {
        public Page2()
        {
            string value = Application.Current.Properties["Test"]?.ToString();
            Label label = new Label() { Text = value ?? "No Value" };
            this.Content = label;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }


    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            NavigationPage nav = new NavigationPage(new Page1());
            this.MainPage = nav;
        }

        protected override void OnStart()
        {
            if(!this.Properties.ContainsKey("Test"))
            this.Properties["Test"] = "Xamarin";
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
